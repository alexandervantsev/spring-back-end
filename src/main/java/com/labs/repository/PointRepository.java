package com.labs.repository;

import com.labs.entity.PointEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.ArrayList;

public interface PointRepository extends CrudRepository<PointEntity,String>{
    ArrayList<PointEntity> findByUserEntity(String userEntity);
}
