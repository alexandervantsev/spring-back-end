package com.labs.controller;

import com.labs.entity.UserEntity;
import com.labs.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Random;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/start")
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.POST, value = "/check")
    public ResponseEntity checkUser(@RequestBody UserEntity userEntity){
        ResponseEntity answer = ResponseEntity.ok(HttpStatus.FOUND);
        switch (userService.checkPassword(userEntity)) {
            case 1: answer = ResponseEntity.ok(HttpStatus.NOT_FOUND); break;
            case 2: answer = ResponseEntity.ok(HttpStatus.CONFLICT); break;
        }
        return answer;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/adduser")
    public ResponseEntity createUser(@RequestBody UserEntity userEntity){

        ResponseEntity answer = ResponseEntity.ok(HttpStatus.OK);
        if (userService.addUser(userEntity.getName(), userEntity.getPassword()) == 1){
            answer = ResponseEntity.ok(HttpStatus.CONFLICT);
        }
        return answer;
    }
    @RequestMapping(value = "/test")
    public int addTest(){
        UserEntity anew = new UserEntity("me","mypass");
        userService.addUser("me","mypass");
        UserEntity nnew = new UserEntity("me","mypass");
        return userService.checkPassword(nnew);

    }


}
