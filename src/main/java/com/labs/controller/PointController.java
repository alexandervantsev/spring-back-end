package com.labs.controller;
import java.util.*;
import com.labs.entity.PointEntity;
import com.labs.service.PointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Random;
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/main")
public class PointController {

    @Autowired
    private PointService pointService;

    @RequestMapping(value="/userpoint/{username}",method = RequestMethod.GET)
    public List<PointEntity> getAllUserPoints(@PathVariable String username){
        return pointService.getAllUserPoints(username);

    }

    @RequestMapping(method = RequestMethod.POST, value = "/addPoint")
    public ResponseEntity savePoint(@RequestBody PointEntity pointEntity){
        PointEntity point = new PointEntity(pointEntity.getX(),pointEntity.getY(),pointEntity.getR(),pointEntity.getUserEntity());
        pointService.addPoint(point);
        return ResponseEntity.ok(HttpStatus.OK);
    }
}
