package com.labs.service;

import com.labs.repository.PointRepository;
import com.labs.entity.PointEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class PointService {
    @Autowired
    private PointRepository pointRepository;

    public List<PointEntity> getAllUserPoints(String username){
        List<PointEntity> result = new ArrayList<>();
        result.addAll(pointRepository.findByUserEntity(username));
        return result;
    }

    public void addPoint(PointEntity pointEntity){
        pointRepository.save(pointEntity);
    }

}
