package com.labs.service;

import com.labs.entity.UserEntity;
import com.labs.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public int addUser(String username, String password){
        int result = 0;
        UserEntity newUser = new UserEntity(username,password);
        if (userRepository.findByName(newUser.getName())==null){
            userRepository.save(newUser);
        } else result = 1;
        return result;
    }

    public int checkPassword(UserEntity userEntity){
        int result = 0;
        if (userRepository.findByName(userEntity.getName()) == null){
            result = 1;
            return result;
        }
        String password = userRepository.findByName(userEntity.getName()).getPassword();
        String _password = userEntity.getHashMD5(userEntity.getPassword());
        if (!password.equals(_password)){
            result = 2;
        }
        return result;
    }
}
