package com.labs.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class PointEntity {
    @Id
    @GeneratedValue
    private int id;
    private double x, y, r;
    private boolean hit;
    private String userEntity;



    public PointEntity(double x, double y, double r, String userEntity){
        this.x = x;
        this.y = y;
        this.r = r;
        this.userEntity = userEntity;
        this.hit = isInside(x,y,r);

    }

    public PointEntity(int id, double x, double y, double r, boolean hit, String userEntity) {
        this.userEntity = userEntity;
        this.id = id;
        this.x = x;
        this.y = y;
        this.r = r;
        this.hit = hit;
    }
    public PointEntity(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }

    public boolean isHit() {
        return hit;
    }

    public void setHit(boolean hit) {
        this.hit = hit;
    }

    public String getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(String userEntity) {
        this.userEntity = userEntity;
    }

    private boolean isInside(final double x, final double y, final double r) {

        boolean result = false;
        if (r > 0) result = ((x <= r/2) && (x >= 0) && (y <= r) && (y >= 0)) // rectangle
                || ((x >= 0) && (y <= 0) && (x*x + y*y <= r*r/4)) // circle
                || ((x <= 0) && (y <= 0) && (2*x + y >= -r));  // triangle
        if (r < 0) result = ((x >= r/2) && (x <= 0) && (y >= r) && (y <= 0)) // rectangle
                    || ((x <= 0) && (y >= 0) && (x*x + y*y <= r*r/4)) // circle
                    || ((x >= 0) && (y >= 0) && (2*x + y <= -r)); //triangle
        if (r==0) result = false;
        return  result;
    }
}
