package com.labs.entity;

import javax.persistence.*;
import java.security.*;
@Entity
public class UserEntity {
    @Id
    @GeneratedValue
    private int id;
    @Column(unique = true)
    private String name;
    private String password;

    public UserEntity(String name, String passwordd) {
        this.name = name;
        this.password = getHashMD5(passwordd);
    }
    public UserEntity(){}
    public int getId() { return id; }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHashMD5(String password){
        try{
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(password.getBytes());

            byte byteData[] = md.digest();

            StringBuilder sb = new StringBuilder();
            for (byte aByteData : byteData) {
                sb.append(Integer.toString((aByteData & 0xff) + 0x100, 16).substring(1));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e ){
            System.out.println("No MD5");
        }
        return null;

    }
}
